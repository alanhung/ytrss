# Ytrss

Command line utility to get ATOM/RSS url from the url of a youtube channel. 

Dual-licensed under [Apache 2.0](LICENSE-APACHE) or [MIT](LICENSE-MIT).
