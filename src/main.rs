use clap::Parser;
use scraper::{Html, Selector};

#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Args {
    url: String,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();
    let response = reqwest::blocking::get(args.url)?.text()?;

    let document = Html::parse_document(&response);
    let selector = Selector::parse(r#"link[title="RSS"]"#).unwrap();

    let channel_id = document
        .select(&selector)
        .next()
        .unwrap()
        .value()
        .attr("href")
        .unwrap();
    println!("{}", channel_id);
    Ok(())
}
